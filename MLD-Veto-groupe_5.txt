﻿UML Veto groupe 5

Espece (#nom:string, description:string) 
Animal (#aid:integer, nom:string, date_naissance:timestamp, num_puce:string, num_passport:string, espece=>Espece.nom, dossier=>Dossier.did) avec nom, espece NOT NULL, dossier UNIQUE
Client (#clid:integer, nom:string, prenom:string, adresse:string, date_naissance:timestamp, tel:integer) avec tous les attributs NOT NULL
Personnel (#peid:integer, nom:string, prenom:string, adresse:string, date_naissance:timestamp, tel:integer, poste:string, specialite=>Espece.nom) avec tous les attributs NOT NULL, poste IN ('veterinaire','assistant')
Dossier (#did: integer)
Mesure(#mid:integer, date_entree:timestamp, taille:float(2), poids:float(2), dossier=>Dossier.did) avec date_entree, dossier,(taille, poids) NOT NULL
Procedure (#pid:integer, date_entree:timestamp, description:string, dossier=>Dossier.did) avec description, date_entree, dossier NOT NULL
Consultation (#cid:integer, date_entree:timestamp, observation:string, dossier=>Dossier.did, editeur=>Personnel.peid) avec observation, date_entree, editeur, dossier NOT NULL
Resultat_analyse (#rid:integer, date_entree:timestamp, lien:url, dossier=>Dossier.did) avec date_entree, lien, dossier NOT NULL, lien UNIQUE
Traitement_prescrit (#tid:integer, date_entree:timestamp, date_debut:timestamp, duree:string, dossier=>Dossier.did, editeur=>Personnel.peid) avec date_debut, date_entree, duree, dossier, editeur NOT NULL, personnel.poste='veterinaire'
Medicament (#molecule:string, description:string) avec description NOT NULL

Animal_traité_par (#date:timestamp, #personnel=>personnel.peid, #animal=>Animal.aid) avec personnel.poste='veterinaire'
Client_possède_animal (#date_debut:timestamp, #date_fin:timestamp, #proprietaire=>client.clid, #animal=>Animal.aid)
Quantité_médacament (#traitement=>Traitement_prescrit.tid, #medicament=>Medicament.molecule, quantite:float(1), unite:string) avec medicament, quantite,unite NOT NULL
Médicament_convient_animal (#medicament=>Medicament.molecule, #espece=>Espece.nom)
