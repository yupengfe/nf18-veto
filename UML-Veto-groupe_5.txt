@startuml

title Relationships - Class Diagram


class Espèce {
  nom: string {key}
  description: string
}

class Animal{
    nom: string NOT NULL
	date de naissance: timestamp
	numéro de sa puce d'identification: string
	numéro de passeport: string
}

class Client{
    adresse: string NOT NULL
    prénom: string NOT NULL
    nom: string NOT NULL
	date de naissance: timestamp NOT NULL
	numéro de téléphone: integer NOT NULL
    }
    
class Personnel{
    nom: string NOT NULL
	prénom: string NOT NULL
	date de naissance: timestamp NOT NULL
	adresse: string NOT NULL
	numero de téléphone: integer NOT NULL
	poste: string NOT NULL
}

class Dossier_medical{

}


class Mesure{
    date d'entrée: timestamp NOT NULL
    taille: float
    poids:float
}

class Procédure{
    date d'entrée: timestamp NOT NULL
    description: string NOT NULL
}

class Consultation{
    date d'entrée: timestamp NOT NULL 
    observation: string NOT NULL 
}

class Résultat_analyse{
    date d'entrée: timestamp NOT NULL 
    lien: string NOT NULL 
}

class Traitement_prescrit{
    date d'entrée: timestamp NOT NULL
    date de début: timestamp NOT NULL 
    durée: string NOT NULL 
}

class Médicament{
    molécule: string {key}
	descrption: string NOT NULL
}


Class Animal_traité_par{
    date: timestamp
}

Class Client_possède_animal{
    date de début:date
    date de fin:string
}

Class Quantité_médacament{
    quantité: float(1) NOT NULL 
    unité: string NOT NULL
}

Class Médicament_convient_animal{
}

Espèce "1" *- "0...*" Animal: < Appartient à
Client "1...*"--"1...*" Animal: Possède >
Personnel "1...*"--"0...*" Animal: < est assisté par
Dossier_medical "1"--"1" Animal: < a
Dossier_medical "1"--"1...*" Traitement_prescrit: contient >
Dossier_medical "1"--"1...*" Mesure: contient >
Dossier_medical "1"--"1...*" Procédure: contient >
Dossier_medical "1"--"1...*" Consultation: contient >
Dossier_medical "1"--"1...*" Résultat_analyse: contient >
Consultation "0...*"--"1" Personnel: < fait
Traitement_prescrit "0...*"--"1" Personnel: < édite
Traitement_prescrit "0...1"--"1...*" Médicament: a >
Médicament "1...*"--"0...*" Espèce: Autorisés pour >
Personnel "0...*"--"1" Espèce: a >

(Animal,Client)..Client_possède_animal
(Animal,Personnel)..Animal_traité_par
(Traitement_prescrit,Médicament)..Quantité_médacament
(Médicament,Espèce)..Médicament_convient_animal

note " taille et poids ne peuvent \nêtre nulls en même temps" as comment2
Mesure..comment2
note " Seul un vétérinaire peut \nprescrire un traitement" as comment3
Traitement_prescrit..comment3
note " Le personnel de la clinique ne doit \npas avoir d'animaux de compagnie \ntraités dans la clinique" as comment4
Personnel..comment4

@enduml